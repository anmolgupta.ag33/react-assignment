import LoginPage from "./pages/LoginPage";

const App = () => {
  return (
    <div className="font-poppins">
      <LoginPage />
    </div>
  );
};

export default App;
