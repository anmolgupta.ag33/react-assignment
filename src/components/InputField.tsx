import { FieldError, UseFormRegister } from "react-hook-form";

interface InputFieldProps {
  label?: string;
  type: string;
  name: string;
  placeholder: string;
  register: UseFormRegister<any>;
  error: FieldError | undefined;
}
const InputField: React.FC<InputFieldProps> = ({
  label,
  type,
  name,
  placeholder,
  register,
  error,
}) => (
  <div className="mb-4 font-inter">
    {label && (
      <label
        className="block text-gray-700 text-sm font-bold mb-2"
        htmlFor={name}
      >
        {label}
      </label>
    )}
    <input
      className="appearance-none border rounded-ll  border-[#464660] border-opacity-50 w-full py-4 px-5 text-sm text-tertiary placeholder:text-opacity-50 leading-tight focus:outline-none focus:shadow-outline"
      type={type}
      placeholder={placeholder}
      {...register(name)}
    />
    {error && <p className="text-red-500 text-xs italic">{error.message}</p>}
  </div>
);

export default InputField;
