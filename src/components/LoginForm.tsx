import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import InputField from "./InputField";

export interface LoginFormInputs {
  email: string;
  password: string;
}

const schema = yup.object().shape({
  email: yup
    .string()
    .email("Please enter a valid email address")
    .required("Email is required"),
  password: yup.string().required("Password is required"),
});

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormInputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: LoginFormInputs) => {
    console.log(data);
  };

  return (
    <form
      className="bg-white  px-8 pt-6 pb-8 mb-4"
      onSubmit={handleSubmit(onSubmit)}
    >
      <InputField
        type="text"
        name="email"
        placeholder="Enter your email"
        register={register}
        error={errors.email}
      />
      <InputField
        type="password"
        name="password"
        placeholder="Enter your password"
        register={register}
        error={errors.password}
      />
      <div className="flex justify-end">
        <p className="text-tertiary text-opacity-50 text-sm">
          Forgot Password?
        </p>
      </div>
      <div className="flex mt-10">
        <button
          className="bg-secondary rounded-ll drop-shadow-[0_20px_32px_rgba(2,1,0,0.42)] hover:bg-opacity-80 w-full text-white font-semibold p-4 text-lg focus:outline-none focus:shadow-outline"
          type="submit"
        >
          Log In
        </button>
      </div>
    </form>
  );
};

export default LoginForm;
