import { BrowserLogo, Logo } from "../components/Icon";
import LoginForm from "../components/LoginForm";
import LeafIcon from "../assets/leaf-icon.svg";

function LoginPage() {
  return (
    <div className="grid grid-cols-12">
      <div className="hidden md:col-span-6 p-4  min-h-screen md:flex flex-col">
        <div className="bg-cover flex-1 px-5 py-6 bg-center bg-[url('assets/Rectangle.png')] rounded-2xl flex flex-col justify-between">
          <Logo />
          <div className="h-52 flex flex-col justify-between">
            <div>
              <p className="text-white text-4xl font-semibold leading-10 mb-1">
                100% Uptime😎
              </p>
              <p className="text-[#BFBFBF] text-3xl font-normal leading-10">
                Zero infrastructure
                <br /> Management
              </p>
            </div>

            <p className="text-[#BFBFBF] text-sm flex items-center">
              <BrowserLogo /> aesthisia.com
            </p>
          </div>
        </div>
      </div>
      <div className="col-span-12 md:col-span-6 min-h-screen p-10 md:p-12 xl:p-16">
        <div className="flex flex-col items-center">
          <img src={LeafIcon} />
          <h1 className="text-secondary text-4xl leading-10 font-medium">
            Welcome <span className="text-primary font-bold">Back!</span>
          </h1>
          <h2
            className="text-tertiary text-sm
      "
          >
            Glad to see you, Again!
          </h2>
        </div>

        <LoginForm />
        <p className="text-center text-tertiary font-inter text-sm">
          Don't an account yet? <span className="text-primary">Sign Up</span>
        </p>
      </div>
    </div>
  );
}

export default LoginPage;
