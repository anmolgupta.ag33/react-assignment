/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["Poppins", "sans-serif"],
      inter: ["Inter", "sans-serif"],
    },
    extend: {
      borderRadius: {
        ll: "10px",
      },
      colors: {
        primary: "#08A593",
        secondary: "#020100",
        tertiary: "#667085",
      },
    },
  },
  plugins: [],
};
